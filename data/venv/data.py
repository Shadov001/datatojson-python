import json
import requests
from bs4 import BeautifulSoup
html_doc = "https://www.gry-online.pl/gry/22"
html_doc2 = "https://www.gry-online.pl/gry/22-2"
html_doc3 = "https://www.gry-online.pl/gry/22-3"
html_doc4 = "https://www.gry-online.pl/gry/22-4"
html_content = requests.get(html_doc).text
html_content2 = requests.get(html_doc2).text
html_content3 = requests.get(html_doc3).text
html_content4 = requests.get(html_doc4).text
page = BeautifulSoup(html_content, 'html.parser')
page2 = BeautifulSoup(html_content2, 'html.parser')
page3 = BeautifulSoup(html_content3, 'html.parser')
page4 = BeautifulSoup(html_content4, 'html.parser')
titles = []
types = []
pTags = page.find_all('p', class_="opis-b")
pTags2 = page2.find_all('p', class_="opis-b")
pTags3 = page3.find_all('p', class_="opis-b")
pTags4 = page4.find_all('p', class_="opis-b")

for pTag in pTags:
    bTags = pTag.find_all('b')
    for bTag in bTags:
        types.append(bTag.text.strip())
for pTag in pTags2:
    bTags = pTag.find_all('b')
    for bTag in bTags:
        types.append(bTag.text.strip())
for pTag in pTags3:
    bTags = pTag.find_all('b')
    for bTag in bTags:
        types.append(bTag.text.strip())
for pTag in pTags4:
    bTags = pTag.find_all('b')
    for bTag in bTags:
        types.append(bTag.text.strip())
for headlines in page.find_all("h5"):
    titles.append(headlines.text.strip())
for headlines in page2.find_all("h5"):
    titles.append(headlines.text.strip())
for headlines in page3.find_all("h5"):
    titles.append(headlines.text.strip())

nestedList = []
json_string = json.dumps(titles)
fullList = dict(zip(titles, types))

for i in range(0,len(titles)):
    nestedList.append({"Title" : titles[i], "Type" : types[i]})
print (nestedList)
print(json.dumps(nestedList, indent = 1))


with open('data.json', 'w') as outfile:
    json.dump(nestedList, outfile)